package com.xiaoan.imeiscaner;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xiaoan.imeiscaner.model.MatchState;
import com.xiaoan.imeiscaner.utils.BeepManager;
import com.xiaoan.imeiscaner.utils.FileUtils;
import com.xiaoan.imeiscaner.utils.SharedPrefsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import cn.bingoogolapple.qrcode.core.QRCodeView;
import cn.bingoogolapple.qrcode.zbar.ZBarView;

public class MainActivity extends AppCompatActivity implements QRCodeView.Delegate {
    private static final String PREFER_CHANGED_FILE_NAME = "prefer_changed_file_name";
    private static final String PREFER_CHANGED_SCAN_MODE = "prefer_changed_scan_mode";
    public static final class ScanMode {
        /**
         * {
         *  "IMEI": "865067022403441"
         *}
         */
        public static final int inputJson = 0;//扫描小安宝盒子

        /**
         * IMEI:865067025550131 SN:20170706-04744
         */
        public static final int mgString = 1;//扫描芒果
        /**
         *
         */
        public static final int chipString = 2;//扫描808芯片
    }
    public static final String DEFAULT_FILE_NAME = "IMEI.txt";
    private QRCodeView mQRCodeView;
    private BeepManager beepManager;
    private String lastText;
    private OutputStreamWriter streamWriter;
    private String currentFilePath;
    private TextView countTextView, contentTxt;
    private int  imeiScanMode = ScanMode.inputJson;
    private static final String [] scanTitle = new String[] {"出库模式", "芒果", "808芯片"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mQRCodeView = (ZBarView) findViewById(R.id.zbarview);
        mQRCodeView.setDelegate(this);

        countTextView = (TextView) findViewById(R.id.count_text_view);
        contentTxt = (TextView) findViewById(R.id.scan_content);

        beepManager = new BeepManager(this);

        FileUtils.createAppDir();

        String changedFileName = SharedPrefsUtils.getStringPref(this, PREFER_CHANGED_FILE_NAME);
        if (TextUtils.isEmpty(changedFileName)) {
            FileUtils.createFile(DEFAULT_FILE_NAME);
        } else {
            FileUtils.createFile(changedFileName);
        }

        changedFileName = TextUtils.isEmpty(changedFileName) ? DEFAULT_FILE_NAME : changedFileName;
        currentFilePath = FileUtils.APP_ROOT_PATH + File.separator + changedFileName;
        try {
            FileOutputStream stream = new FileOutputStream(currentFilePath, true);
            streamWriter = new OutputStreamWriter(stream);
        } catch (FileNotFoundException e) {

        }

        int totalCount = FileUtils.match(currentFilePath, "aaaaaaaa").count;
        if (countTextView != null) {
            countTextView.setText(String.valueOf(totalCount));
        }

        imeiScanMode = SharedPrefsUtils.getIntPref(this, PREFER_CHANGED_SCAN_MODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        menu.findItem(R.id.change_mode).setTitle(scanTitle[imeiScanMode]);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.change_mode) {
            imeiScanMode = (imeiScanMode + 1) % 3;
            item.setTitle(scanTitle[imeiScanMode]);
            Toast.makeText(this, scanTitle[imeiScanMode],
                    Toast.LENGTH_SHORT).show();
            SharedPrefsUtils.setIntPref(this, PREFER_CHANGED_SCAN_MODE, imeiScanMode);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onScanQRCodeSuccess(String result) {
        String retStr = null;
        if (imeiScanMode == ScanMode.inputJson) {
            try {
                retStr = new JSONObject(result).getString("IMEI");
            } catch (JSONException e) {}
        } else if (imeiScanMode == ScanMode.mgString){
            String [] handleArr = result.split(" ");
            retStr = handleArr[0].split(":")[1] + " " + handleArr[1].split(":")[1];
        } else {
            retStr = result;
        }

        if (contentTxt != null) {
            contentTxt.setText(retStr);
        }

        if(retStr == null || retStr.equals(lastText)) {
            // Prevent duplicate scans
            return;
        }
        //// FIXME: 简单的限定一下,最好根据特征限制
        if (imeiScanMode == ScanMode.chipString && (!retStr.contains("IMEI") ||
            !retStr.contains("SN"))) {
            return;
        }
        MatchState matchState = FileUtils.match(currentFilePath, retStr);
        if (matchState.matched) {
            return;
        }

        lastText = retStr;

        //beepManager.playBeepSoundAndVibrate();
        if (streamWriter != null) {
            try {
                streamWriter.write(retStr + "\r\n");
                streamWriter.flush();
                if (countTextView != null) {
                    countTextView.setText(String.valueOf(matchState.count + 1));
                }
            } catch (IOException e) {

            }
        }
    }

    @Override
    public void onScanQRCodeOpenCameraError() {

    }

    public void clearClick(View view) {
        new AlertDialog.Builder(this)
                .setTitle("确认清除掉已经扫描的记录吗")
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FileUtils.clearFileContent(currentFilePath);
                        lastText = "";
                        if (countTextView != null) {
                            countTextView.setText("0");
                        }
                    }
                })
                .setNegativeButton("取消", null)
                .show();
    }

    public void changeClick(View view) {
        final EditText text = new EditText(this);
        final File file = new File(currentFilePath);
        text.setText(file.getName().replace(".txt", ""));
        new AlertDialog.Builder(this)
                .setTitle(R.string.scaner_input_file_name)
                .setView(text)
                .setPositiveButton(R.string.scaner_change_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (TextUtils.isEmpty(text.getText().toString())) {
                            return;
                        }
                        String newName = text.getText().toString().concat(".txt");
                        if (newName.equals(file.getName())) {
                            return;
                        }
                        File dstFile = new File(FileUtils.APP_ROOT_PATH + "/" + newName);
                        if (file.exists()) {
                            file.renameTo(dstFile);
                            file.delete();
                        } else {
                            try {
                                file.createNewFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        currentFilePath = FileUtils.APP_ROOT_PATH + "/" + newName;
                        SharedPrefsUtils.setStringPref(MainActivity.this, PREFER_CHANGED_FILE_NAME,
                                newName);
                    }
                })
                .setNegativeButton(R.string.scaner_cancel, null)
                .show();
    }

    public void shareClick(View view) {
        Intent intent = new Intent(Intent.ACTION_SEND);

        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(currentFilePath)));
        intent.setType("text/plain");
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        startActivity(Intent.createChooser(intent, getResources().getText(R.string.send_to)));
    }

    @Override
    protected void onStart() {
        super.onStart();
        mQRCodeView.startCamera();

        mQRCodeView.showScanRect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mQRCodeView.startSpot();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mQRCodeView.stopSpot();
    }

    @Override
    protected void onStop() {
        mQRCodeView.stopCamera();
        super.onStop();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return mQRCodeView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (streamWriter != null) {
            try {
                streamWriter.flush();
                streamWriter.close();
            } catch (IOException e) {

            }
        }
        mQRCodeView.onDestroy();
    }
}
