package com.xiaoan.imeiscaner.model;

/**
 * Created by xunce on 2017/6/19.
 */

public class MatchState {
    public boolean matched;
    public int count;

    public MatchState(boolean matched, int count) {
        this.matched = matched;
        this.count = count;
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
