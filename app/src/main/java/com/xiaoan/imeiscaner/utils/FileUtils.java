package com.xiaoan.imeiscaner.utils;

import android.os.Environment;
import android.text.TextUtils;

import com.xiaoan.imeiscaner.model.MatchState;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by xunce on 2017/6/19.
 */

public class FileUtils {
    public static final String APP_ROOT_PATH = Environment.getExternalStorageDirectory().getPath()
            + "/XiaoAn/Scanner";

    private FileUtils() {
    }

    public static void createAppDir() {
        File file = new File(APP_ROOT_PATH);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static void createFile(String fileName) {
        if (TextUtils.isEmpty(fileName)) {
            return;
        }
        File file = new File(APP_ROOT_PATH + File.separator + fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {

            }
        }
    }

    public static void clearFileContent(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return;
        }
        File file = new File(filePath);
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file);
            writer.print("");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static boolean isFileExists(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return false;
        }
        File file = new File(filePath);
        return file.exists();
    }

    public static MatchState match(String filePath, String content)  {
        File file = new File(filePath);
        MatchState state = new MatchState(false, 0);
        if (file.exists()) {
            FileInputStream fis = null;
            InputStreamReader isr = null;
            try {
                fis = new FileInputStream(file);
                isr = new InputStreamReader(fis, "utf-8");
            } catch (IOException e) {
                return state;
            }

            BufferedReader br = new BufferedReader(isr);
            StringBuffer sb = new StringBuffer();
            String data = null;

            try {
                while (!TextUtils.isEmpty(data = br.readLine())) {
                    sb.append(data + "\n");
                }
            } catch (IOException e) {}

            List<String> list = new ArrayList<>(Arrays.asList(sb.toString().split("\n")));
            List<String> emptyList = Collections.singletonList("");
            if (list == null || list.isEmpty()) return state;
            list.removeAll(emptyList);

            state.setCount(list.size());
            state.setMatched(list.indexOf(content) != -1);
            return state;
        }
        return state;
    }

    public static void deleteFile(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return;
        }
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }
}
